Highcharts.chart('container', {
    chart: {
        type: 'column'
    },
    title: {
        text: ''
    },
    subtitle: {
        text: ''
    },
    xAxis: {
        categories: [
            'Костанайская область',
            'Западно-Казахсканская область',
            'г. Нур-Cултан',
            'г. Алматы',
            'Южно-Казахстанская область',
            'Восточно-Казахстанская область'
        ],
        crosshair: true
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Количество талонов'
        }
    },
    tooltip: {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
            '<td style="padding:0"><b>{point.y}</b></td></tr>',
        footerFormat: '</table>',
        shared: true,
        useHTML: true
    },
    plotOptions: {
        column: {
            pointPadding: 0,
            borderWidth: 0
        }
    },
    series: [{
        name: '',
        data: [800, 710, 106, 129, 144, 176],
        color: '#2755BD'

    }]
});


