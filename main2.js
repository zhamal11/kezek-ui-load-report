function getYoutubeLikeToDisplay(millisec) {
    var seconds = (millisec / 1000).toFixed(0);
    var minutes = Math.floor(seconds / 60);
    var hours = "";
    if (minutes > 59) {
        hours = Math.floor(minutes / 60);
        hours = (hours >= 10) ? hours : "0" + hours;
        minutes = minutes - (hours * 60);
        minutes = (minutes >= 10) ? minutes : "0" + minutes;
    }

    seconds = Math.floor(seconds % 60);
    seconds = (seconds >= 10) ? seconds : "0" + seconds;
    if (hours != "") {
        return hours + ":" + minutes + ":" + seconds;
    }
    return minutes + ":" + seconds;
}
console.log(getYoutubeLikeToDisplay(234532456))

let time = getYoutubeLikeToDisplay(234532456)
Highcharts.chart('container2', {
    chart: {
        type: 'column'
    },
    title: {
        text: ''
    },
    subtitle: {
        text: ''
    },
    xAxis: {
        categories: [
            'Галиева Дина',
            'Избасарова Динара',
            'Кадешова Асемгул',
            'Мирзагаиева Шара',
            'Сагингалиев Иляс',
            'Тулешова Гулсезим',
        ],
        crosshair: true
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Время (час)'
        }
    },
    tooltip: {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        pointFormatter: function () {
            
            return '<span style="color:' + this.series.color + ';">\u25CF</span> ' + this.series.name + ' - ' + time;
        },
        footerFormat: '</table>',
        shared: true,
        useHTML: true
    },
    plotOptions: {
        column: {
            pointPadding: 0.2,
            borderWidth: 0
        }
    },
    series: [{
        name: 'Время обслужование',
        data: [49, 34, 56.5, 34, 44.2, 12],
        color: '#00A6ED'

    }, {
        name: 'Время сеанса',
        data: [24.5, 48, 38.5, 53.4, 16.0, 42.5],
        color: '#F6511D'

    }]
});

Highcharts.chart('container3', {
    chart: {
        type: 'column'
    },
    title: {
        text: ''
    },
    subtitle: {
        text: ''
    },
    xAxis: {
        categories: [
            'Галиева Дина',
            'Избасарова Динара',
            'Кадешова Асемгул',
            'Мирзагаиева Шара',
            'Сагингалиев Иляс',
            'Тулешова Гулсезим',
        ],
        crosshair: true
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Количество талонов'
        }
    },
    tooltip: {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
            '<td style="padding:0"><b>{point.y}</b></td></tr>',
        footerFormat: '</table>',
        shared: true,
        useHTML: true
    },
    plotOptions: {
        column: {
            pointPadding: 0.2,
            borderWidth: 0
        }
    },
    series: [{
        name: 'Талон',
        data: [49, 71, 106, 129, 144, 176],
        color: '#2755BD'

    }]
});
